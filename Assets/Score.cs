using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public int value;

    private void Start()
    {
        value = PlayerPrefs.GetInt("highscore", 0);
    }

    private void OnDestroy()
    {
         PlayerPrefs.SetInt("highscore", value);
         PlayerPrefs.Save();
    }

}
