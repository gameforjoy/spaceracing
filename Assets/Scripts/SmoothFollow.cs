﻿using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    public float distance = 10.0f;
    public float height = 5.0f;
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;
    public float distanceDamping = 20.0f;
    public float currentDistance;


    public Transform target;


    private float m_refPos;
    private float m_refRot;
 
    public float  smoothTime;
    public float speed = 500f;
    private Vector3 velocity = Vector3.zero;

    private void LateUpdate()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition  = target.TransformPoint(new Vector3(0, height, -distance));

        // Smoothly move the camera towards that target position
        //transform.position = new Vector3(target.position.x, transform.position.y, transform.position.z);
        smoothTime = 0.1f;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        //transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        transform.LookAt(target);
    }
}