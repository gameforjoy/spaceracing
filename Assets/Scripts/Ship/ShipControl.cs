using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControl : MonoBehaviour
{
    [SerializeField] private float _speedZ;
    [SerializeField] private float _speedXFactor;
    [SerializeField] private GameObject _shipModel;
    [SerializeField] private float _boundary;
    [SerializeField] private DynamicJoystick _dynamicJoystick;
    private Vector3 _newRotRight;
    private Vector3 _newRotLeft;
    private Vector3 _newRotZero;
    private Quaternion _quatnewRotRight;
    private Quaternion _quatnewRotLeft;
    private Quaternion _quatnewRotZero;
    float smoothing = 5f;

    private float _startposition;
    private float _speedX;
    private uint _distance;
    private uint _speedFactor = 1;

    private int _currentLevel = 1;
 
    private SmoothFollow _smoothFollow;
    private void Start()
    {
        _newRotRight = new Vector3(0f, 0f, -30f);
        _quatnewRotRight = Quaternion.Euler(_newRotRight);

        _newRotLeft = new Vector3(0f, 0f, 30f);
        _quatnewRotLeft = Quaternion.Euler(_newRotLeft);

        _newRotZero = new Vector3(0f, 0f, 0f);
        _quatnewRotZero = Quaternion.Euler(_newRotZero);
        _startposition = transform.position.z;

        _smoothFollow = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SmoothFollow>();


    }
    private void Update()
    {
        Move();
        _distance = (uint)(transform.position.z - _startposition) / 10;
    }

    public string GetDistance
    {
        get => _distance.ToString();
    }

    public uint GetDistanceInt
    {
        get => _distance;
    }
    public float Speed
    {
        get => _speedZ;
    }

    public float SetSpeed
    {
        set { _speedZ = value; }
    }

    /// <summary>
    /// ���������� �������� �������
    /// </summary>
    private void Move()
    {
        _speedX = _speedXFactor * _dynamicJoystick.Horizontal;
        //if (Input.GetKey(KeyCode.D)) MoveX(1);
        //else if (Input.GetKey(KeyCode.A)) MoveX(-1);
        //else MoveX(0);  // ��� ������������ ��������� ���� ������ �� ������... �������, �������� ��� ������� �����
        if (_speedX > 0) MoveX(1);
        else if (_speedX < 0) MoveX(-1);
        else MoveX(0); 

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + _speedZ * _speedFactor * Time.deltaTime);
    }



    /// <summary>
    /// ����� ������� �����(-1) � ������(1)
    /// </summary>
    /// <param name="directional"></param>
    public void MoveX(int directional)
    {

        switch (directional)
        {

            case 1:
                _shipModel.transform.rotation = Quaternion.Lerp(_shipModel.transform.rotation, _quatnewRotRight, smoothing * Time.deltaTime);
                if (transform.position.x < _boundary)
                    transform.position = new Vector3(transform.position.x + _speedX * Time.deltaTime, transform.position.y, transform.position.z);
                break;

            case -1:
                _shipModel.transform.rotation = Quaternion.Lerp(_shipModel.transform.rotation, _quatnewRotLeft, smoothing * Time.deltaTime);
                if (transform.position.x > -_boundary)
                    transform.position = new Vector3(transform.position.x + _speedX * Time.deltaTime, transform.position.y, transform.position.z);
                break;

            case 0:
                _shipModel.transform.rotation = Quaternion.Lerp(_shipModel.transform.rotation, _quatnewRotZero, smoothing * Time.deltaTime);
                
                break;
            default:
                break;
        }
    }

    public void Boost(bool boost)
    {
        if (boost)
        {
            _speedZ *= 3;
            _smoothFollow.height = 0.2f;
            _smoothFollow.distance = 1f;
        }
        else
        {
            _speedZ /= 3;
            _smoothFollow.height = 2f;
            _smoothFollow.distance = 5f;
        }
    }

    public void OnLevelIncrease(int level)
    {
        _speedZ *= 1.1f;
        _currentLevel = level;
    }
}


