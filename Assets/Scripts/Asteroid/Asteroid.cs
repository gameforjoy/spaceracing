using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Asteroid : MonoBehaviour
{
  //  [SerializeField] private ParticleSystem  _burstEffect;
    [SerializeField] private float _rotateSpeed;
   // [SerializeField] private GameObject _gameOverCanvas;
   //  private GameObject _camera;

   // public AudioSource newSong;
 //   public AudioSource deathSong;
 //   bool PlayedDeathSong = false;
 //   private bool _restartTimerOn = false;
  //  float _restartTime = 3;

    private void LateUpdate()
    {
        transform.Rotate(0, _rotateSpeed * Time.deltaTime, 0);
    }

    //private void Update()
    //{
    //    if (_restartTimerOn)
    //    {
    //        _restartTime -= Time.deltaTime;
    //        if (_restartTime <= 0)
    //        {
    //            _restartTimerOn = false;
    //            //SceneManager.LoadScene(0);
    //            _gameOverCanvas.SetActive(true);
    //        }
    //    }    
    //}



    private void OnTriggerEnter(Collider other)
    {
       // if (other.gameObject.CompareTag("Ship"))
        
        if (other.TryGetComponent(out Ship ship))
        {  
            Debug.Log("Collision with "+other.gameObject.name+" Detected");
            
            GameObject _camera = GameObject.FindGameObjectWithTag("MainCamera");
            _camera.GetComponent<SmoothFollow>().target = gameObject.transform;
            
            other.gameObject.SetActive(false);

            Debug.Log("Asteroid as Target for camera is assigned");
            GameOver gameOver  = ship.GetComponentInParent<GameOver>();
            gameOver.FinishGame();


            //ParticleSystemRenderer rrenderer = Instantiate(_burstEffect, transform.position, _burstEffect.transform.rotation).GetComponent<ParticleSystemRenderer>();
         
            //other.gameObject.SetActive(false);
            //GameObject _parentShip = other.gameObject.transform.parent.gameObject;
            //_parentShip.GetComponent<ShipControl>().SetSpeed = 0;
            //_camera = GameObject.FindGameObjectWithTag("MainCamera");
            //_camera.GetComponent<SmoothFollow>().target = gameObject.transform;
            //_restartTimerOn = true;
        }

    }


}
