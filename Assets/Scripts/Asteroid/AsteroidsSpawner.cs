using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsSpawner : MonoBehaviour
{
    [SerializeField] private Asteroid _asteroid;
    [SerializeField] private float _spawnDeltaZ;
    [SerializeField] private float _spawnWidth;

    private float _spawnDeltaTime;
    private float _spawnTimer = 0;
    private ShipControl _shipControl;
    //  private int _currentLevel = 1;
    private void Start()
    {
        _shipControl = FindObjectOfType<ShipControl>();
    }
    private void Update()
    {
        _spawnTimer += Time.deltaTime;
        _spawnDeltaTime = _spawnDeltaZ / _shipControl.Speed;
        if (_spawnTimer >= _spawnDeltaTime)
        {
            SpawnAsteroid();
            _spawnTimer = 0;
        }

    }
    private void SpawnAsteroid()
    {
        Vector3 _spawnAsterPoint = transform.position;
        _spawnAsterPoint.x = Random.Range(-_spawnWidth / 2, _spawnWidth / 2);
        Instantiate(_asteroid, _spawnAsterPoint, Quaternion.identity);
    }

    public void OnLevelIncrease(int level)
    {
        _spawnDeltaZ /= 1.05f; 
       // _currentLevel = level;
    }
}
