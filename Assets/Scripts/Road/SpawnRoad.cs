using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnRoad : MonoBehaviour
{
    [SerializeField] private GameObject _roadBlock;
    [SerializeField] private Asteroid _asteroid;
    [SerializeField] private Transform _startSpawnPoint;
    private Vector3 _spawnPoint;
    private Vector3 _spawnAsterPoint;
    public GameObject[] _roadBlocks;
    private Asteroid[] _asteroids;
    private ShipControl _shipControl;
   
    private int _indxBlock = 0;
    private int _indxAster = -1;
   // private int Level;

    private float _roadBlockSizeZ;
    private float _roadBlockSizeX;
    private float deltaPosition = 50f;
    private float _spawnAsterDistance = 50f;
    private float _startShipPosition;
    private float ddeltaPosition;
    private void Start()
    {
        _roadBlocks = new GameObject[30];
        _asteroids = new Asteroid[50];

        // ������ ������ ����� ������
        MeshRenderer meshR = _roadBlock.GetComponent<MeshRenderer>();
        _roadBlockSizeZ =  meshR.bounds.size.z;
        _roadBlockSizeX = meshR.bounds.size.x;

        _spawnPoint = _startSpawnPoint.position;
        _spawnAsterPoint = _startSpawnPoint.position;
        _spawnAsterPoint.y = _spawnAsterPoint.y + 5;
        #region InitSpawn
        
    
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        #endregion

        _shipControl = GetComponent<ShipControl>();
        _startShipPosition = _shipControl.transform.position.z;
        ddeltaPosition = 10 + Mathf.Round(Random.value * 100);
    }

    private void Update()
    {

        if (_shipControl.transform.position.z > _roadBlocks[1].transform.position.z) 
        {
            DestroyBlock(_roadBlocks);
            _roadBlocks[_indxBlock] = SpawnBlock(ref _spawnPoint, _roadBlocks);
        }



        // ddeltaPosition = 100 + Mathf.Round(Random.value*100);
        if (_shipControl.transform.position.z > (_startShipPosition + ddeltaPosition))
        {
            //Debug.Log(_startShipPosition);
           // Debug.Log(ddeltaPosition);
            _startShipPosition = _shipControl.transform.position.z;
          //  float deltaPosition = Mathf.Round(Random.value * 30);
            //_spawnAsterPoint.z = spawnAsterPoint.z
          
            ddeltaPosition = 10 + Mathf.Round(Random.value * 100);
        }




    }

    private GameObject SpawnBlock(ref Vector3 _spawnPoint, GameObject[] roadBlocks)
    {

        roadBlocks[_indxBlock] = Instantiate(_roadBlock, _spawnPoint, Quaternion.identity);
        _spawnPoint = new Vector3(roadBlocks[_indxBlock].transform.position.x, roadBlocks[_indxBlock].transform.position.y, roadBlocks[_indxBlock].transform.position.z + _roadBlockSizeZ);
        _indxBlock += 1;
        return roadBlocks[_indxBlock-1];
    }



    private void DestroyBlock(GameObject[] roadBlocks)
    {
        Destroy(roadBlocks[0]);
        
        for(int i=0; i<_indxBlock-1; i++)
        {
            roadBlocks[i] = roadBlocks[i + 1];
        }
        _indxBlock -= 1;
    }



}
