using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class LevelIncreaseClass : UnityEvent<int> { }
public class LevelUp : MonoBehaviour
{
    [SerializeField] private float _levelDuration;
    private float _levelDurationTimer = 0;

   // public event UnityAction<int> LevelIncrease;
    public  LevelIncreaseClass LevelIncrease;
    private int _currentLevel=1;

    private void Update()
    {
        _levelDurationTimer += Time.deltaTime;
         if (_levelDurationTimer >= _levelDuration)
        {
            _currentLevel += 1;
            LevelIncrease?.Invoke(_currentLevel);
            _levelDurationTimer = 0;
        }
    }

    


}
