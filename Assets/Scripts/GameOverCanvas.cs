using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverCanvas : MonoBehaviour
{
    // [SerializeField] private GameObject _gameCanvas;
    [SerializeField] public TMP_Text distance;
    [SerializeField] public TMP_Text score;
    public void OnBtnRetryBtnDown()
    {
        SceneManager.LoadScene(0);
    }

    public void OnBtnExitDown()
    {
        Application.Quit();
    }
}
