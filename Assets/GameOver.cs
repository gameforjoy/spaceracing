using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    [SerializeField] private ParticleSystem _burstEffect;
    [SerializeField] private GameObject _gameOverCanvas;
    [SerializeField] private GameObject _gameCanvas;
    [SerializeField] private AudioSource _backgroundSong;
    [SerializeField] private AudioSource _deathSong;
    [SerializeField] private AudioSource _explosion;
    [SerializeField] private Score _score;


    private float _effectTime = 3;
    private bool _restartTimerOn = false;

    private void Update()
    {
        if (_restartTimerOn)
        {
            _effectTime -= Time.deltaTime;
            if (_effectTime <= 0)
            {
                _restartTimerOn = false;
                //SceneManager.LoadScene(0);
              //  _gameCanvas.SetActive(false);
                _gameOverCanvas.SetActive(true);
                int curDistance = int.Parse(_gameCanvas.GetComponent<GameCanvas>().distance.text);
                _gameOverCanvas.GetComponent<GameOverCanvas>().distance.text = curDistance.ToString();
                // int curScore = int.Parse(_gameOverCanvas.GetComponent<GameOverCanvas>().score.text);
                //   if (int(_gameOverCanvas.GetComponent<GameOverCanvas>().score.text)>2)
                _gameOverCanvas.GetComponent<GameOverCanvas>().score.color = Color.red;
                if (curDistance > _score.value)
                {
                    _score.value = curDistance;
                    _gameOverCanvas.GetComponent<GameOverCanvas>().score.color = Color.green;
                    _gameOverCanvas.GetComponent<GameOverCanvas>().distance.color = Color.green;
                }
                
                _gameOverCanvas.GetComponent<GameOverCanvas>().distance.text = curDistance.ToString();
                _gameOverCanvas.GetComponent<GameOverCanvas>().score.text = _score.value.ToString();
                //_gameOverCanvas.GetComponent<GameOverCanvas>().score.color = Color.green;
                _gameCanvas.SetActive(false);
            }
        }
    }
    public void FinishGame()
    {
        ParticleSystemRenderer rrenderer = Instantiate(_burstEffect, transform.position, _burstEffect.transform.rotation).GetComponent<ParticleSystemRenderer>();

      //  gameObject.SetActive(false);
        GetComponent<ShipControl>().SetSpeed = 0;
        _backgroundSong.Stop();
        _explosion.Play();
        _deathSong.Play();

        _restartTimerOn = true;
    }
}
