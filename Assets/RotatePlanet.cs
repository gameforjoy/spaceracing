using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlanet : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed;
    Vector3 _rotateAxis = new Vector3(0.3f, 1f, 0.3f);
    private void FixedUpdate()
    {
        transform.Rotate(_rotateAxis, _rotateSpeed * Time.fixedDeltaTime, 0);
    }
}
