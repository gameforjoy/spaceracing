using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameCanvas : MonoBehaviour
{
    [SerializeField] private ShipControl _shipControl;
    private bool _isLeftPressed;
    private bool _isRightPressed;
    private bool _isBoostPressed;
    [SerializeField] public TMP_Text distance;
    [SerializeField] public Score  score;

    private float _maxDoublePressTime = 1.0f;
    private float _doublePressTimer = 0;
    private bool _doubleClick = false;


   


    private void Update()
    {
        if (_isLeftPressed) _shipControl.MoveX(-1);
        if (_isRightPressed) _shipControl.MoveX(1);
       
        distance.text = _shipControl.GetDistance;
        if (_shipControl.GetDistanceInt > score.value)
            distance.color = Color.red;


    }

    public void OnBtnRightDown()
    {
        OnBtnDown(ref _isRightPressed);
    }
    public void OnBtnRightUp()
    {
        OnBtnUp(ref _isRightPressed);
    }

    public void OnBtnBoostDown()
    {
        _shipControl.Boost(true);
    }
    public void OnBtnBoostUp()
    {
        _shipControl.Boost(false);
    }

    public void OnBtnLeftDown()
    {
        OnBtnDown(ref _isLeftPressed);
    }
    public void OnBtnLeftUp()
    {
        OnBtnUp(ref _isLeftPressed);
    }

    #region Press Button Handler
    private void OnBtnDown(ref bool isPressed)
    {
        isPressed = true;
    }
    private void OnBtnUp(ref bool isPressed)
    {
        isPressed = false;
    }
    private void OnBtnExit(ref bool isPressed)
    {
        isPressed = false;
    }
    #endregion
}
